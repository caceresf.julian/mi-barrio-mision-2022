import React from "react";
import "./inicio.css";
import { Link } from "react-router-dom";

export default function Inicio() {
  return (
    <div className="container">
      <div className="inicio__main">
        <div className="inicio__descripcion">
          <h1 className="inicio__titulo">MiBarrio.com</h1>
          <h4 className="inicio__subtitulo">
            ¡El futuro ha llegado a tu negocio!
          </h4>
          <p className="pt-md-5">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto,
            eveniet. Consequatur facilis, expedita laboriosam soluta ducimus
            perspiciatis reprehenderit voluptatem corporis, sint laudantium
            nesciunt odio magni ab neque quibusdam dolorem placeat pariatur
            asperiores molestiae. Deserunt amet impedit ad in illum? Repellat
            amet officia aperiam eum error, ad exercitationem vero deleniti
            reprehenderit?
          </p>
          <h4 className="inicio__subtitulo">¿Ya estás registrado?</h4>
          <div className="d-flex justify-content-between justify-content-md-center align-items-center mt-2">
            <Link to="/login" className="inicio__botones">
              <button
                type="button"
                className="btn btn-primary d-flex flex-column justify-content-center align-items-center"
              >
                <i className="fa fa-star inicio__icono" aria-hidden="true"></i>
                ¡Por supuesto!
              </button>
            </Link>
            <Link to="/registro">
              <button
                type="button"
                className="btn btn-danger d-flex flex-column justify-content-center align-items-center"
              >
                <i
                  className="fa fa-exclamation-triangle inicio__icono"
                  aria-hidden="true"
                ></i>
                No, registrarme
              </button>
            </Link>
          </div>
        </div>
        <div className="inicio__imagen d-none d-lg-block" />
      </div>
    </div>
  );
}
