import React from "react";
import { Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import brandLogo from './brand-logo.svg';

export default function Navegacion() {
  return (
    <Navbar className="navigation-bar" expand="lg">
      <Container>
        <Navbar.Brand className="d-flex align-items-center">
        <Link to="/inicio"><img src={brandLogo} alt="mibarrio.com" style={{ width: '50px'}} /></Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto align-items-center">
            <Link to="/inicio">Inicio</Link>
            <Link className="px-3" to="/registro">Registro</Link>
            <Link to="/login">Login</Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
