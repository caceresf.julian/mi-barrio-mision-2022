import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navegacion from "./navegacion/Navegacion";
import Inicio from "./inicio/Inicio";
import Login from "./login/Login";
import Registro from "./registro/Registro";


export default function Router() {
  return (
    <BrowserRouter>
      <Navegacion/>
      <Switch>
        <Route exact path="/inicio" component={Inicio}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/registro" component={Registro} />
        <Route path="*" component={Inicio} />
      </Switch>
    </BrowserRouter>
  );
}
