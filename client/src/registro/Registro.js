import React, { useState } from "react";
import axios from "axios";
import "./registro.css";
import swal from "sweetalert";

export default function Registro() {
  const [nombre, setNombre] = useState("");
  const [correo, setCorreo] = useState("");
  const [password, setPassword] = useState("");
  const [passwordVer, setPasswordVer] = useState("");

  function login(e) {
    e.preventDefault();
    try {
      const userData = {
        nombre,
        correo,
        password,
        passwordVer,
      };

      axios.post("http://localhost:4000/user/signup", userData).then((res) => {
        if (res.status === 200) {
          swal("Registrado", "Ya puedes loggearte.", "success");
        } else {
          swal("Error", res.data, "error");
        }
      });
    } catch (err) {
      swal(err);
    }
    console.log(nombre, correo, password, passwordVer);
  }

  return (
    <div className="register">
      <div className="card register-box">
        <div className="card-body">
          <form
            className="d-flex flex-column justify-content-between h-100 py-3"
            onSubmit={login}
          >
            <h4 className="text-center">Registro</h4>
            <input
              className="form-control"
              type="text"
              placeholder="Nombre de la tienda"
              onChange={(e) => setNombre(e.target.value)}
            ></input>
            <input
              className="form-control"
              type="text"
              placeholder="Correo electrónico"
              onChange={(e) => setCorreo(e.target.value)}
            ></input>
            <input
              className="form-control"
              type="password"
              placeholder="Contraseña"
              onChange={(e) => setPassword(e.target.value)}
            ></input>
            <input
              className="form-control"
              type="password"
              placeholder="Escribe tu contraseña de nuevo"
              onChange={(e) => setPasswordVer(e.target.value)}
            ></input>
            <button type="submit" className="btn btn-dark">
              Registrarse
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
