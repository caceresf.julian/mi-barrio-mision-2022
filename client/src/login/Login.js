import React, { useState } from "react";
import "./login.css";

export default function Login() {
  const [correo, setCorreo] = useState("");
  const [password, setPassword] = useState("");

  function login(e) {
    e.preventDefault();
    console.log(correo, password);
  }

  return (
    <div className="login">
      <div className="card login-box">
        <div className="card-body">
          <form
            className="d-flex flex-column justify-content-between h-100 py-3"
            onSubmit={login}
          >
            <h4 className="text-center">Loggeo</h4>
            <input
              className="form-control"
              type="text"
              placeholder="Email"
              onChange={(e) => setCorreo(e.target.value)}
            ></input>
            <input
              className="form-control"
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            ></input>
            <button type="submit" className="btn btn-dark">
              Log in
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
