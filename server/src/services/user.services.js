const pool = require("../database/database");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const userSignup = async (user) => {
  const { correo, password, passwordVer, nombre } = user;

  //Validación
  if (!correo || !password || !passwordVer || !nombre) {
    return { error: "Por favor, llene el formulario por completo." };
  }

  if (password !== passwordVer) {
    return { error: "Revisa que la contraseña esté correctamente verificada." };
  }

  const res = await pool.query(
    `SELECT * FROM tienda WHERE correo='${correo}';`
  );

  if (res.rows.length > 0) {
    return {
      error: "Hay un usuario existente con dicho correo electrónico.",
    };
  }

  //Encripción de contraseña
  const salt = await bcrypt.genSalt();
  const passwordHash = await bcrypt.hash(password, salt);
  
  //Guardar usuario en la db
  const savedUser = await pool.query(`INSERT INTO tienda (correo, password, nombre) VALUES ('${correo}', '${passwordHash}', '${nombre}')`);

  if(savedUser) return { mensaje: `Tienda "${nombre}" creada exitosamente.`} 
  else return { error: `Hay un error en la base de datos. ${savedUser}`};
   
};

const userLogin = async (user) => {
  const { correo, password } = user;

  //Validación
  if (!correo || !password ) {
    return { error: "Por favor, llene el formulario por completo." };
  }

  const usuarioExistente = await pool.query(`SELECT * FROM tienda WHERE correo='${correo}'`);

  if (usuarioExistente.rows.length == 0) return { error: "Email o contraseña incorrectos."};

  const passwordsIguales = await bcrypt.compare(
    password, usuarioExistente.rows[0].password
  )

  if(!passwordsIguales) return { error: "Email o contraseña incorrectos."};

  //Crear token
  const token = jwt.sign({ correo }, process.env.JWT_SECRET);

  return token;

}

const userValidation = (cookie) => {
  try {
    if(!cookie) return { error: "No autorizado."}

    jwt.verify(cookie, process.env.JWT_SECRET);

    const {payload} = jwt.decode(cookie, { complete : true});
    const {correo} = payload;

    return {mensaje: "Autorizado", correo };

  } catch (error) {
    return { error: "No autorizado."};
  }
}

module.exports = {
  userSignup,
  userLogin,
  userValidation
};
