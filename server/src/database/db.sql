-- Table: public.tienda

-- DROP TABLE public.tienda;

CREATE TABLE IF NOT EXISTS public.tienda
(
    correo text COLLATE pg_catalog."default" NOT NULL,
    password text COLLATE pg_catalog."default" NOT NULL,
    nombre text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tienda_pkey PRIMARY KEY (correo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tienda
    OWNER to xrdwhmko;