const userServices = require("../services/user.services");

const createUser = (req, res) => {
  try {
    userServices.userSignup(req.body).then((ans) => {
      if (ans.error) res.status(202).send(ans.error);
      else res.status(200).send(ans.mensaje);
    });
  } catch (error) {
    console.log(error);
  }
};

const loginUser = (req, res) => {
  try{
    userServices.userLogin(req.body).then((ans) => {
      if (ans.error) res.status(202).send(ans.error);
      else res.cookie("token", ans, {httpOnly: true}).send();
    })
  } catch (error){
    console.log(error);
  }
}

const verifyUser = (req, res) => {
  try {
    userServices.userValidation(req.cookies.token).then((ans) => {
      if (ans.error) res.status(401).send(ans.error);
      else res.json(ans);
    })
  } catch (error) {
    console.log(error);
  }
}

const logoutUser = async (req, res) => {
  res.cookie("token", "", {httpOnly: true, expires: new Date(0)}).send();
}

module.exports = {
  createUser,
  loginUser,
  verifyUser,
  logoutUser
};
