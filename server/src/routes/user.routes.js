const router = require("express").Router();
const userControllers = require("../controllers/user.controllers");

router.post("/signup", userControllers.createUser );
router.post("/login", userControllers.loginUser );
router.get("/loggedin", userControllers.verifyUser );
router.get("/logout", userControllers.logoutUser );

module.exports = router;
