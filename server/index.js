const express = require("express");
const app = express();
const PORT = process.env.PORT || 4000;
const cors = require("cors");
const cookieParser = require("cookie-parser");

require("dotenv").config({ path: "./.env" });

app.use(
  cors({
    origin: ["http://localhost:3000"],
    credentials: true,
  })
);

app.use(express.json({ limit: "5mb" }));

app.use(cookieParser());

app.use('/user', require('./src/routes/user.routes'));

app.listen(PORT, () => {
  console.log("Ready on port", PORT);
});
