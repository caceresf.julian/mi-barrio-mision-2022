# mi-barrio-mision-2022
This a fullstack project developed by Julián Cáceres Flórez for Misión 2022.

For the front-end, the app is using React and, for the backend, Node.js, Express and PostgreSQL. 

## Project status
This project is still on progress. So, stay tuned!